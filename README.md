# fieldwork-flowmeter

![Image](https://gitlab.com/zenquest/fieldwork-flowmeter/blob/master/Zen_Quest_Flowmeter_2019.JPG)

Version: 1.0

License: GPL 3, this covers all files and information in the repository.

A flow-meter for river studies fieldwork aimed at IB, GCSE, IGCSE and A Level student use, built on Arduino with 3D printed parts. It can perform the following two functions:
* Measure velocity - the speed which water is moving through the unit in kilometres per hour.
* Measure flow rate - how much water (volume of water) is flowing through the unit in litres per minute.

Software required:

Install the Arduino IDE, you can get it here: https://www.arduino.cc

Download and install the following libraries:

* https://github.com/adafruit/Adafruit_ILI9341 - version=1.3.1
* https://github.com/adafruit/Adafruit_TouchScreen - version=1.0.1
* https://github.com/adafruit/Adafruit-GFX-Library - version=1.4.2
* https://github.com/prenticedavid/MCUFRIEND_kbv - version=2.9.9-Beta
* https://github.com/adafruit/TFTLCD-Library - 8ad1ede on Feb 22, 2018

Library install location:

* Mac install location -      /Users/YOURUSERNAME/Documents/Arduino/libraries
* Linux systems maybe something like: ~/Documents/Arduino/libraries
* Windows install location - C:\Users\YOURUSERNAME\Documents\Arduino\libraries

Parts list:
* 1 DN80 water sensor 3"
* 1 Arduino Uno R3 (clones also work)
* 1 HiLetgo 2.4 Inch TFT LCD Display Shield Touch Panel ILI9341 240X320 for Arduino UNO MEGA
* 1 length of 3 strand wire (we used silicon copper wire as its really soft and flexible)
* 1 set of 3D printed parts
* 1 Length of 19mm stainless steel tube (ours is 50cm but you can change this to your needs)
* 1 USB Power bank
* 1 USB cable
* 6 20mm m3 counter sunk wood screws
* Some shrink wrap tubes in multiple sizes
* Epoxy resin
* Supper glue
* Sold
* Optional 8mm micro spirit level.
* Cost should be under €25.00 for all the parts.

Tools list:
* 3D printer (we print in PETG and the parts have been optimised for this material)
* Soldering Station with hot air gun and helping hands
* Wire cutters
* Long nose pliers
* Philips screw driver.

Video of early software testing: https://www.youtube.com/watch?v=qXciaDOqzO4

Arduino Sketch written by: Steven Zimmer - https://slzimmer.com and hardware designed by Zen Quest Adventures Ltd. https://zenquest.co.
